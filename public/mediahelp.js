var OrbitalType = "1s";
var rorbit = 3.0;
var nquant = 1;
var norm = 1.0; clight = 1;
var wavefct = 'Math.exp(-r)';
var nsizex = 256; nsizey = 256;
var bckcol = 0.0;
var animon = 0;
var sid = 0;



//
// Dummy Function
function doNothing() {
  console.log("Hey, there");
}
//
// reduziere auf Ganzzahl zwischen 0 und 255
function byteRange(a) {
	if (a > 255) {
	  a = 255;
	}
	if (a < 0) {
	  a = 0;
	}
	return Math.floor(a);
}
//
//  function to build the spectra
function createOrbital(ilight) {
  var imgMod, ix, iy, newset
  var psi = 0;
  if (ilight > 0) {
    clight = ilight;
  }
//
// build the image
var canspec = document.getElementById("myCanvasSpec");
canspec.width = nsizex;
canspec.height = nsizey;
var ctxs = canspec.getContext("2d");
var imgMod = ctxs.createImageData(nsizex, nsizey);
maxrho = 0; pis = 0;
// console.log(wavefct,maxrho);
for (ix = 0; ix < nsizex; ix++) {
    xpos = (ix/nsizex-0.5)*rorbit;
    for (iy = 0; iy < nsizey; iy++) {
      zpos = (iy/nsizey-0.5)*rorbit;
      ypos = (Math.random()-0.5)*rorbit;
      r = Math.sqrt(xpos*xpos+ypos*ypos+zpos*zpos)
      dl = xpos/r;
      dm = ypos/r;
      dn = zpos/r;

      if (OrbitalType == "1s")      { psi = Math.exp(-r);}
      if (OrbitalType == "2s")      { psi = (2-r)*Math.exp(-r/2);}
      if (OrbitalType == "2px")     { psi = r*Math.exp(-r/2)*dl;}
      if (OrbitalType == "2py")     { psi = r*Math.exp(-r/2)*dm;}
      if (OrbitalType == "2pz")     { psi = r*Math.exp(-r/2)*dn;}
      if (OrbitalType == "3s")      { psi = (27 - 18*r + 2*power(r,2))*Math.exp(-r/3);}
      if (OrbitalType == "3dz2-r2") { psi = power(r,2)*Math.exp(-r/3)*(dn*dn-(dm*dm+dl*dl)/2);}
      if (OrbitalType == "3dx2-y2") { psi = power(r,2)*Math.exp(-r/3)*(dl*dl-dm*dm);}
      if (OrbitalType == "3dxz")    { psi = power(r,2)*Math.exp(-r/3)*dl*dn;} 
      if (OrbitalType == "Box")     { psi = Math.sin(nquant*Math.PI*((xpos+rorbit/2)/rorbit))*Math.sin(nquant*Math.PI*((zpos+rorbit/2)/rorbit))*Math.sin(nquant*Math.PI*((ypos+rorbit/2)/rorbit));} //*Math.sin(nquant*Math.PI*(ypos+rorbit/2)/rorbit/2)*Math.sin(nquant*Math.PI*(zpos+rorbit/2)/rorbit/2));}           


      rho = clight*norm*psi*psi;
   
      if (psi < 0) {
        rhor = rho;
        rhob = 0;
        rhog = 0;
      }
      if (psi > 0) {
        rhor = 0;
        rhob = rho;
        rhog = 0;
      }    
      if (psi == 0) {
        rhor = 0;
        rhob = 0;
        rhog = 0;
      }    
      newset = (nsizex * iy + ix) * 4;
      imgMod.data[newset] = byteRange(rhor*255);
      imgMod.data[newset + 1] = byteRange(rhog*255);
      imgMod.data[newset + 2] = byteRange(rhob*255);
      imgMod.data[newset + 3] = byteRange(rho*255);
   //   console.log(xpo,ypos,zpos,psi)

    }
  }
 // console.log(wavefct,maxrho);
//          ctxp.clearRect(0, 0, img.width, img.height);
ctxs.putImageData(imgMod, 0, 0);
}
//
// Formatting text output
// function txtout(txtstr,elemstr) {
//  var nxtline = document.createElement('br');
//  var element = document.getElementById(elemstr).appendChild(nxtline); 
//  var wrtline = document.createTextNode(txtstr);
//  var element = document.getElementById(elemstr).appendChild(wrtline);
// }
function txtout(txtstr,elemstr) {
  var list = document.getElementById(elemstr);
  while (list.hasChildNodes()) {
    list.removeChild(list.firstChild);
  }
  var wrtline = document.createTextNode(txtstr);
  var element = document.getElementById(elemstr).appendChild(wrtline);
  }
//
// Power-Function
function power(x, z) {
  var y;
  y = Math.pow(x, z);
  return y;
} 
//
// Formatting Number
function form(x, z) {
  var y;
  x = Math.round(x * power(10, z));
  y = x / power(10, z);
  return y;
}
//
// Alerting the Info-Block
function getInfo() {
  alert('The webApp displays the probability density of an electron in a sphere or a box.' + 
         ' The App uses the wavefuntion of a particle based on one electron atomic orbitals.' +
         ' The color (red/blue) determines the phase of the wavefunction (+/-).' +
         ' With the slider you can choose 1s,2s,2, 3s, some 3d and a particle-in-the- box with n=3 wavefunctions. \n \n' + 
         'Some infos can be find here: \n' + 
         'https://de.wikipedia.org/wiki/Atomorbital \n' +
         'https://de.wikipedia.org/wiki/Teilchen_im_Kasten' );
}
//
// Alerting the Help-Block
function getHelp() {
  alert('1. Choose an orbital type with the first slider \n' + 
         '2. Press "Settings" to animate the orbial image \n' +
         '3. Press "Catalog" to print the orbital \n' +
         '4. Press "About" for a short explanation. \n' +
         '5. Press "Help" for this info. \n' +
         '6. Press "Home" to reload the webApp. \n' +
         '7. Touch the image to start/stop the animation. \n' +
         '6. Use the second slider to change intensity. \n' +                  
         ' \n' ); 
}


  function setOrbitSlide(getType) {

    OrbitalSelect = getType;
    
    if (OrbitalSelect == "1") {
        nquant = 1
        rorbit = 3
        norm = 2;
        wavefct = 'Math.exp(-r)';
        OrbitalType = "1s"
        }

        if (OrbitalSelect == "2") {
          nquant = 2;
          rorbit = 20;
          norm = 9;
          wavefct = '(2-r)*Math.exp(-r/2)';
          OrbitalType = "2s"
          }        
  
        if (OrbitalSelect == "3") {
          nquant = 2;
          rorbit = 20;
          norm = 9;
          wavefct = 'r*Math.exp(-r/2)*dl';
          OrbitalType = "2px"
          }
  
          if (OrbitalSelect == "4") {
            nquant = 2;
            rorbit = 20;
            norm = 9;
            wavefct = 'r*Math.exp(-r/2)*dm';
            OrbitalType = "2py"
          }

          if (OrbitalSelect == "5") {
            nquant = 2;
            rorbit = 20;
            norm = 9;
            wavefct = 'r*Math.exp(-r/2)*dn';
            OrbitalType = "2pz"
          }

          if (OrbitalSelect == "6") {
            nquant = 3;
            rorbit = 40;
            norm = 0.15;
    //   psi = 1/(sqrt(6*pi)*81)*(Z/a0)^(3/2)*(Z*r/a0)^2*exp(-Z*r/3/a0)*(dn*dn-(dm*dm+dl*dl)/2)         
            wavefct = '(27 - 18*r + 2*power(r,2))*Math.exp(-r/3)';
            OrbitalType = "3s"
            } 
  
    if (OrbitalSelect == "7") {
          nquant = 3;
          rorbit = 40;
          norm = 0.2;
  //   psi = 1/(sqrt(6*pi)*81)*(Z/a0)^(3/2)*(Z*r/a0)^2*exp(-Z*r/3/a0)*(dn*dn-(dm*dm+dl*dl)/2)         
          wavefct = 'power(r,2)*Math.exp(-r/3)*(dn*dn-(dm*dm+dl*dl)/2)';
          OrbitalType = "3dz2-r2"
          }     
          
          if (OrbitalSelect == "8") {
            nquant = 3;
            rorbit = 40;
            norm = 0.2;
            wavefct = 'power(r,2)*Math.exp(-r/3)*(dl*dl-dm*dm)';
            OrbitalType = "3dx2-y2"
            }   

            if (OrbitalSelect == "9") {
              nquant = 3;
              rorbit = 40;
              norm = 1;
              wavefct = 'power(r,2)*Math.exp(-r/3)*dl*dn';
              OrbitalType = "3dxz"
            }  

            if (OrbitalSelect == "10") {
              nquant = 3;
              rorbit = 1;
              norm = 1;
              wavefct = 'Math.cos(Math.PI*x)*Math.cos(Math.PI*y)*Math.cos(Math.PI*z)';
              OrbitalType = "Box"
            }  
  
  
        var wrtline = document.createTextNode("selected " + OrbitalType);
        var element = document.getElementById('OrbitalType');  
        element.replaceChild(wrtline, element.firstChild);      
  
        createOrbital(clight);
    }

    function animateOrbital() { 
        var elem = document.getElementById("myCanvasSpec"); 

        if (animon == 0) {  
          var id = setInterval(frame, 40); 
          sid = id;  
          var animonn = 1;
        }  
        if (animon == 1) {     
          clearInterval(sid);
          var animonn = 0; 
        }    
        animon = animonn 
        function frame() {  
            createOrbital(0);
            elem.style.top = 0;
        }
      }
